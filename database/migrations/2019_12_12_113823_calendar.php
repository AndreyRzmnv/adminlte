<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Calendar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->bigInteger('autor_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->dateTime('start');
            $table->dateTime('end');
            $table->tinyInteger('status')->default(0);
            $table->dateTime('staus_date')->nullable();
            $table->timestamps();
            
            $table->foreign('autor_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
        });
        /*
        Schema::create('calendar_events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->timestamps();
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendars');
        /*Schema::dropIfExists('calendar_events');*/
    }
}
