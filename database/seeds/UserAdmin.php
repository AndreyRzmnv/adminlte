<?php

use Illuminate\Database\Seeder;

class UserAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin1',
            'email' => 'admin@admin1',
            'hash' => Hash::make('123456789'),
            'password' => bcrypt('123456789')
        ]);
    }
}
