<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Calendar;
use App\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
class StatisticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('statistics.index', compact('users'));
    }
    public function view()
    {
        $data = Calendar::all();
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $difference = array();
        $count = collect();
        $countMas = array();
        $dates = [];


        
        $end = Carbon::parse($request->input('end'));
        $end->setTime(23, 59, 59);
        $newEnd = $end->format('Y-m-d H:i:s', $end);
        //
        //все даты в промежутке
        $period = CarbonPeriod::create($request->input('start'), $request->input('end'));
        foreach ($period as $key => $date) 
        {
            $dates[] = $date->format('Y-m-d');
        }
        
        if($request->input('user') == 'all')
        {
            $data = Calendar::with('user')->where('status', 1)->whereBetween('updated_at', [$request->input('start'), $newEnd])->get();
        }else{
            $data = Calendar::with('user')->where('status', 1)->where('user_id', $request->input('user'))->whereBetween('updated_at', [$request->input('start'), $newEnd])->get();
        }
        $data->map(function($item, $key)
        {
            $item['difference'] = [];
        });

        foreach ($data as $key => $value)
        {
            foreach ($dates as $key2 => $value2)
            {
                $difference[$key2] = 0;
                $countMas[$value->user->id][$value2] = 0;
                if($value->staus_date == $value2)
                {
                    $difference[$key2] = Carbon::parse($value->start)->floatDiffInHours($value->updated_at);
                }
            }
            $value->difference = $difference;    
        }

        foreach ($data as $key => $value) 
        {
            foreach ($dates as $key2 => $value2) 
            {
                if($value->staus_date == $value2)
                {
                    $countMas[$value->user->id][$value2] += 1;
                    $countMas[$value->user->id]['name'] = $value->user->name;
                }
            }
            $count = $countMas;
        }
        return [
            'dates' => $dates,
            'data' => $data,
            'count' => $count
        ];
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
