<?php

namespace App\Http\Controllers;
//use Illuminate\Support\Facades\DB;
use App\User;
use App\Role;
use App\Role_user;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Yajra\Datatables\Datatables;
use App\Http\Requests\UserUpdateRequest;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        //dd($user);
        
        return view('users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function data()
    {   
            return Datatables::of(User::query())
            ->addColumn('edit', function($edit){
                $edit = $edit->id;
                return view('plugins.edit_button', compact('edit'));
            })
            ->addColumn('delete', function($delete){
                $delete = $delete->id;
                return view('plugins.delete_button', compact('delete'));
            })
            ->rawColumns(['edit','delete'])
            ->make(true);
    }
    public function create()
    {
        $roles = Role::all();
        return view('users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {   
        //dd($request->role);
        
        $user = User::create($request->all());
        $user->attachRole($request->role);
        flash('Пользователь создан')->success();
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    //public function edit(User $user)
    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(User $user, UserUpdateRequest $request)
    {
       // dd($user);
        flash('Данные обновленны')->success();
        $user->update($request->all());
        //return $user;
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {

        $user->delete();
        return 'Пользователь удален';
    }
}
