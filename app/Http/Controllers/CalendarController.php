<?php

namespace App\Http\Controllers;
use App\User;
use App\Role_user;
use App\Calendar;
use App\Events\CalendarEvent;
use Illuminate\Http\Request;
use App\Http\Requests\CalendarRequest;
use Illuminate\Support\Facades\Input;

class CalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = User::all();
        $user = auth()->user();
        if($user->hasRole('Manager')){
            $users = User::whereRoleIs('Employee')->get();
        }

        return view('calendar', compact('user', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function view(Request $request)
    {   
        $user = auth()->user();
        if($user->hasRole('Administrator|Manager')){
            $data = Calendar::with('user')->whereBetween('start', [$request->input('start'), $request->input('end')])->get();
        }else{
            $data = Calendar::with('user')->where('user_id', $user->id)->whereBetween('start', [$request->input('start'), $request->input('end')])->get();//special data
            if($user->hasRole('Employee')){
                $data->map(function($item, $key)
                {
                    $item['editable'] = false;
                });
                
            }
            
        }        
        //status color
        /*
        foreach ($data as $key => $value) {
            if($data[$key]['status'] == 1){
                $data[$key]['backgroundColor'] = '#68ea4a';
                $data[$key]['borderColor'] = '#68ea4a';
                $data[$key]['editable'] = false;
            }
        }*/

        $data->map(function($item, $key)
        {

            $item['title'] = '[' . $item->user->name . ']' . $item['title'];
            if($item['status'] == 1){
                $item['backgroundColor'] = '#68ea4a';
                $item['borderColor'] = '#68ea4a';
                $item['editable'] = false;
            }

        });
        return $data;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CalendarRequest $request)
    {
        Calendar::create($request->all());
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Calendar  $calendar
     * @return \Illuminate\Http\Response
     */
    public function show(Calendar $calendar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Calendar  $calendar
     * @return \Illuminate\Http\Response
     */
    public function edit(Calendar $calendar)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Calendar  $calendar
     * @return \Illuminate\Http\Response
     */
    public function update(Calendar $calendar, CalendarRequest $request)
    {
        $user = auth()->user();
        $calendar->update($request->all());
        return 'ok';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Calendar  $calendar
     * @return \Illuminate\Http\Response
     */
    public function destroy(Calendar $calendar)
    {
        $calendar->delete();
        return 'ok';
    }
}
