<?php

namespace App\Console\Commands;
use App\Calendar;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Events\CalendarEvent;
class EventCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Calendar:event';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Event';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public $data;
    public function __construct()
    {
        parent::__construct();
        $time = date('Y-m-d H:i:s');
        $newTime = Carbon::parse($time);
        $newTime->setSeconds('00');
        $this->data = Calendar::where('start', $newTime->format('Y-m-d H:i:s'))->get();
        
        
    }

    /**
     * Execute the console command. 
     *
     * @return mixed
     */
    public function handle()
    {
        $data = Carbon::now('Europe/Moscow');
        foreach($this->data as $val){
            $user = User::find($val->user_id);
            $this->data = $val;
            $this->channel = $user->hash;
            //\Log::info($data . " -- Cron is working fine!");
            event(
                new CalendarEvent($this->channel, $this->data)
            );
        }
        
        
    }
}
