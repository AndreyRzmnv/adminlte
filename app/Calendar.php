<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;
use Carbon\Carbon;

class Calendar extends Model
{
    use LaratrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'autor_id',
        'user_id',
        'status',
        'start', 
        'end',
        'staus_date'
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function setEndAttribute($end)
    {
        $newEnd = Carbon::parse($end);
        if($newEnd->toTimeString() == '00:00:00'){
            $newEnd->setTime(23, 59, 59);
        }
        $this->attributes['end'] = $newEnd->format('Y-m-d H:i:s', $end);
    }
    public function setStartAttribute($start)
    {
        $newEnd = Carbon::parse($start);
        $this->attributes['start'] = $newEnd->format('Y-m-d H:i:s', $start);
    }
    public function set_Staus_dateAttribute($staus_date)
    {
        $newEnd = Carbon::parse($staus_date);
        $this->attributes['staus_date'] = $newEnd->format('Y-m-d', $staus_date);
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    /*protected $hidden = [
        'password', 'remember_token',
    ];*/

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    /*protected $casts = [
        'email_verified_at' => 'datetime',
    ];*/
}
