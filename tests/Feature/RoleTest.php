<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
class RoleTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAdmin()
    {
        $user = User::find(1);
        $response = $this->actingAs($user)->get('/home');
        $response->assertStatus(200);
        $response->assertSee('Пользователи');
        $response->assertSee('Добавить пользователя');
        $response->assertSee('Календарь');
        $response->assertSee('Статистика');
        $response = $this->actingAs($user)->get('/users');
        $response->assertStatus(200);
        $response = $this->actingAs($user)->get('/calendar');
        $response->assertSee('Добавление событий');
        $response->assertStatus(200);
        $response = $this->actingAs($user)->get('/statistics');
        $response->assertStatus(200);
    }
    public function testManager()
    {
        $user = User::find(2);
        $response = $this->actingAs($user)->get('/home');
        $response->assertStatus(200);
        $response->assertDontSee('Пользователи');
        $response->assertDontSee('Добавить пользователя');
        $response->assertSee('Календарь');
        $response->assertDontSee('Статистика');
        $response = $this->actingAs($user)->get('/users');
        $response->assertStatus(403);
        $response = $this->actingAs($user)->get('/calendar');
        $response->assertSee('Добавление событий');
        $response->assertStatus(200);
        $response = $this->actingAs($user)->get('/statistics');
        $response->assertStatus(403);
    }
    public function testEmployee()
    {
        $user = User::find(3);
        $response = $this->actingAs($user)->get('/home');
        $response->assertStatus(200);
        $response->assertDontSee('Пользователи');
        $response->assertDontSee('Добавить пользователя');
        $response->assertSee('Календарь');
        $response->assertDontSee('Статистика');
        $response = $this->actingAs($user)->get('/users');
        $response->assertStatus(403);
        $response = $this->actingAs($user)->get('/calendar');
        $response->assertDontSee('Добавление событий');
        $response->assertStatus(200);
        $response = $this->actingAs($user)->get('/statistics');
        $response->assertStatus(403);
    }

}
