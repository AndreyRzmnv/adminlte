<?php

namespace Tests\Feature;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;


class ControllerTest extends TestCase
{
    //use WithoutMiddleware;
    //use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    
    //UserController

    public function testControllerUserIndex()
    {

        $this->withoutMiddleware([
            \App\Http\Middleware\Authenticate::class,
            \Laratrust\Middleware\LaratrustRole::class,
        ]);
        $res= $this->get('/home');
        $res->assertStatus(200);
    }
    public function testControllerUserLogin(){
        $res = $this->post('login', array(
            'email' => 'admin@admin',
            'password' => '123456789'
        ));
        $res->assertStatus(302);
        $res->assertRedirect('/home');
    }
    public function testControllerUserCreate()
    {
        
        $this->withoutMiddleware([
            \App\Http\Middleware\Authenticate::class,
            \Laratrust\Middleware\LaratrustRole::class,
        ]);
        
        $this->withoutMiddleware(ExampleMiddleware::class);
        $res = $this->post('/users', array(
            'name' => 'test',
            'email' => 'test@test',
            'password' => '123456789',
            'role' => 1
        ));
        $res->assertSessionHasNoErrors();
        $res->assertStatus(302);
        $res->assertRedirect('/users');
        
    }
    
    public function testControllerUserEdit()
    {
        $this->withoutMiddleware([
            \App\Http\Middleware\Authenticate::class,
            \Laratrust\Middleware\LaratrustRole::class,
        ]);
        $user = User::max('id');
        $path = '/users/' . $user;
        $res= $this->patch($path, array(
            'name' => 'test1',
            'email' => 'test1@test1'
        ));
        $res->assertSessionHasNoErrors();
        $res->assertStatus(302);
        $res->assertRedirect('/users');
        
    }
    
    public function testControllerUserDelete()
    {
        $this->withoutMiddleware([
            \App\Http\Middleware\Authenticate::class,
            \Laratrust\Middleware\LaratrustRole::class,
        ]);
        $user = User::max('id');
        $path = '/users/' . $user;
        $res= $this->call('DELETE', $path);
        $res->assertSessionHasNoErrors();
        $res->assertStatus(200);
        $res->assertSee('Пользователь удален');
        
    }
    //StatisticsController
    public function testControllerStatisticsIndex()
    {

        $this->withoutMiddleware([
            \App\Http\Middleware\Authenticate::class,
            \Laratrust\Middleware\LaratrustRole::class,
        ]);
        $res= $this->get('/statistics');
        $res->assertStatus(200);
    }

    public function testControllerStatisticsStore()
    {

        $this->withoutMiddleware([
            \App\Http\Middleware\Authenticate::class,
            \Laratrust\Middleware\LaratrustRole::class,
        ]);
        $res= $this->post('/statistics', array(
            'start' => '2020-01-17',
            'end' => '2020-01-18'
        ));
        $res->assertSee('"dates":["2020-01-17","2020-01-18"]');
        $res->assertStatus(200);
    }
    




    //CalendarController

    public function testControllerCalendarIndex()
    {

        $user = User::find(1);
        $res= $this->actingAs($user)->get('/calendar');
        $res->assertSessionHasNoErrors();
        $res->assertSee('Добавление событий');
        $res->assertStatus(200);
        
    }
    
}