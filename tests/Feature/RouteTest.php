<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RouteTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testLogin()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    public function testUsers()
    {
        $response = $this->get('/users');
        $response->assertRedirect('/');
    }
    public function testHome()
    {
        $response = $this->get('/home');
        $response->assertRedirect('/');
    }
    public function testCalendar()
    {
        $response = $this->get('/calendar');
        $response->assertRedirect('/');
    }
    public function testStatistics()
    {
        $response = $this->get('/statistics');
        $response->assertRedirect('/');
    }
}
