<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
class ValidationTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    //User
    public function testControllerValidationUserCreate()
    {
        
        $this->withoutMiddleware([
            \App\Http\Middleware\Authenticate::class,
            \Laratrust\Middleware\LaratrustRole::class,
        ]);
        
        $this->withoutMiddleware(ExampleMiddleware::class);
        $res= $this->post('/users', array(
            'name' => 'test',
            'email' => 'testtest',
            'password' => '123456789',
            'role' => 1
        ));
        $res->assertSessionHasErrors();

        $res= $this->post('/users', array(
            'name' => '',
            'email' => '',
            'password' => '',
            'role' => 1
        ));
        $res->assertSessionHasErrors();
        
    }
    public function testControllerValidationUserEdit()
    {
        $this->withoutMiddleware([
            \App\Http\Middleware\Authenticate::class,
            \Laratrust\Middleware\LaratrustRole::class,
        ]);
        $user = User::max('id');
        $path = '/users/' . $user;
        $res= $this->patch($path, array(
            'name' => 'test1',
            'email' => 'test1test1'
        ));
        $res->assertSessionHasErrors();

        $res= $this->patch($path, array(
            'name' => '',
            'email' => ''
        ));
        $res->assertSessionHasErrors();
        
    }
}