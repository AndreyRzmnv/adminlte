<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;

class ContentTest extends TestCase
{
    use WithoutMiddleware;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testHome()
    {
        $response = $this->get('/home')
            ->assertSee('Dashboard')
            ->assertSee('You are logged in!')
            ->assertStatus(200);
    }
    
    public function testUsers()
    {
        $response = $this->get('/users')
            ->assertSee('Список пользователей')
            ->assertStatus(200);
    }
    //--------
    public function testUsersCreate()
    {
        $user = User::find(1);
        $response = $this->get('/users/create', array('user' => $user))
            ->assertStatus(500);
    }
    
    
    public function testCalendar()
    {
        $response = $this->get('/calendar')
        ->assertStatus(500);
    }
    
    public function testStatistics()
    {
        $response = $this->get('/statistics')
            ->assertStatus(200);
    }
    
}
