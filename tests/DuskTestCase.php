<?php

namespace Tests;

use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Laravel\Dusk\TestCase as BaseTestCase;
use Appstract\DuskDrivers\Opera\SupportsOpera;
use Appstract\DuskDrivers\Safari\SupportsSafari;

abstract class DuskTestCase extends BaseTestCase
{
    use CreatesApplication, SupportsOpera, SupportsSafari;

    
    /**
     * Prepare for Dusk test execution.
     *
     * @beforeClass
     * @return void
     */
    

    public static function prepare()
    {
        static::startChromeDriver();
        //static::startOperaDriver();
        //static::startSafariDriver();
    }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return \Facebook\WebDriver\Remote\RemoteWebDriver
     */
    protected function driver()
    {
        $options = (new ChromeOptions)->addArguments([
            //'--disable-gpu',
            //'--headless',
            '--window-size=1920,1080',
        ]);
        /*
        return RemoteWebDriver::create(
            'http://localhost:9515', DesiredCapabilities::opera()
        );
        */
        /*
        return RemoteWebDriver::create(
            'http://localhost:9515', DesiredCapabilities::safari()
        );
        */
        
        return RemoteWebDriver::create(
            'http://localhost:9515', DesiredCapabilities::chrome()->setCapability(
                ChromeOptions::CAPABILITY, $options
            )
        );
    }
}
