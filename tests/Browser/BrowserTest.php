<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use App\User;
use App\Calendar;
use Carbon\Carbon;

class BrowserTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    
    //loginTest
    public $id;
    public $event_id;
    public function testLogin()
    {
        Browser::macro('typeDate', function ($selector, $year, $month, $day) {
            $this->keys($selector, $day)
                ->keys($selector, $month)
                ->keys($selector, $year);
                return $this;
        });
        $this->browse(function ($first, $second) {
            $second->loginAs(User::find(2))
                ->visit('/calendar');
            //loginTest
            //Не правильный логин
            $first->visit('/')
                ->type('email', 'admin@admin1')
                ->type('password', '123456789')
                ->press('Вход')
                ->assertPathIs('/')
                ->assertSee('auth.failed')
            //Не правильный пароль
            ->visit('/')
                ->type('email', 'admin@admin')
                ->type('password', '1234567891')
                ->press('Вход')
                ->assertPathIs('/')
                ->assertSee('auth.failed')
            //Правельный логин и пароль
            ->visit('/')
                ->type('email', 'admin@admin')
                ->type('password', '123456789')
                ->press('Вход')
                ->assertPathIs('/home')
                ->assertSee('You are logged in!')
            //userTest
            ->clickLink('Пользователи')
            ->assertSee('Список пользователей')
            //Создание пользовтеля
            ->visit('/users/create')
                ->type('name', 'test')
                ->type('email', 'test@test')
                ->type('password', '1234567891')
                ->select('role', 'Employee')
                ->press('Регистрация')
                ->assertPathIs('/users')
                ->assertSee('Пользователь создан');
            $this->id = User::max('id');//id созданного пользователя
            $date = Carbon::now();//текущая дата
            //Изменение пользователя
            $first->visit('/users/' . $this->id . '/edit')
                ->type('name', 'test1')
                ->type('email', 'test1@test1')
                ->press('Обновить')
                ->assertPathIs('/users')
                ->assertSee('Данные обновленны')
            //calendarTest
            //Добавление события
            ->visit('/calendar')
                //->pause(1000)
                ->waitForText('Добавление событий')
                ->assertSee('Добавление событий')
                ->typeDate('#input_date_start', $date->year, $date->month, $date->day)
                ->typeDate('#input_date_end',  $date->year, $date->month, $date->day + 1)
                ->select('role', $this->id)
                ->type('#input_event', 'test')
                ->press('Добавить')
            //Изменение события
            ->visit('/calendar')
                ->waitForText('[test1]test')
                ->assertSee('[test1]test')
                ->clickLink('[test1]test')
                ->waitForText('Изменение')
                ->assertSee('Изменение')
                ->typeDate('#edit_date_start', $date->year, $date->month, $date->day + 1)
                ->typeDate('#edit_date_end', $date->year, $date->month, $date->day + 2)
                ->type('#edit_event', 'test1')
                ->press('#update-but')
            //Удаление события
            //->visit('/calendar')
                ->waitForText('[test1]test1')
                ->assertSee('[test1]test1')
                ->clickLink('[test1]test1')
                ->waitForText('Изменение')
                ->assertSee('Изменение')
                ->press('#del-but')
                ->pause(1000)
                ->assertDontSee('[test1]test1');
            //statisticsTest
            //создание события для статистики    
            $first->typeDate('#input_date_start', $date->year, $date->month, $date->day)
                ->typeDate('#input_date_end',  $date->year, $date->month, $date->day + 1)
                ->select('role', 2)
                ->type('#input_event', 'statistics')
                ->press('Добавить')
                ->waitForText('statistics')
                ->clickLink('statistics')
                ->waitForText('Изменение')
                ->assertSee('Изменение')
                ->press('#success-but')
                ->waitForText('[Manager]statistics')
                ->assertSee('[Manager]statistics');
                //socket тест
            $second->waitForText('[Manager]statistics', 10)
                ->assertSee('[Manager]statistics');
            $first->clickLink('Статистика')
                //->waitForText('[test1]test1')
                ->waitForText('Статистика(События/Часы)')
                ->waitForText('Статистика(События/кол-во)')
                ->assertSee('Статистика(События/Часы)')
                ->assertSee('Статистика(События/кол-во)')
                ->typeDate('#statistics_start', $date->year, $date->month, $date->day - 1)
                ->typeDate('#statistics_end', $date->year, $date->month, $date->day + 1)
                ->press('#chartTime')
            
                //Выход
            ->clickLink('Выход')
                ->waitForText('Вход в систему')
                ->assertSee('Вход в систему');
        });
    }
    public function testClear()
    {
        $this->event_id = Calendar::max('id');
        $event = Calendar::find($this->event_id);
        if($event->title == 'statistics'){
            $event->delete();
        }
        $this->id = User::max('id');
        $user = User::find($this->id);
        if($user->name == 'test1' || $user->name == 'test'){
            $user->delete();
        }
    }
}
