@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    
@stop

@section('content')
<div class="container-floid row">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js" integrity="sha256-TQq84xX6vkwR0Qs1qH5ADkP+MvH0W+9E7TdHJsoIQiM=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" integrity="sha256-R4pqcOYV8lt7snxMQO/HSbVCFRPMdrhAFMH+vr9giYI=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css" integrity="sha256-aa0xaJgmK/X74WM224KMQeNQC2xYKwlAt08oZqjeF0E=" crossorigin="anonymous" />
    
    <div class="col-3 box box-primary p-2 m-1">
        <label>Дата начала</label>
        <input class="form-control" id="statistics_start" type="date" name="start"><br>
        <label>Дата конца</label>
        <input class="form-control" id="statistics_end" type="date" name="end"><br>
        <label>Пользователь</label>
        <select id="select-user" name="role" class="custom-select" required>
            <option value="all">Все</option>
            @foreach($users as $val)
                <option value="{{ $val->id }}">{{ $val->name }}</option>
            @endforeach
        </select>
        <button id="chartTime" type="submit" class="btn btn-primary">Открыть</button>
    
    </div>
    <div class="col-4 box box-primary p-2 m-1">
        <h3 class="box-title">Статистика(События/Часы)</h3>
        <canvas class="canvas" id="myChartEventTime" width="400" height="400"></canvas>
    </div>
    <div class="col-4 box box-primary p-2 m-1">
        <h3 class="box-title">Статистика(События/кол-во)</h3>
        <canvas class="canvas" id="myChartEventCount" width="400" height="400"></canvas>
    </div>
    <script>
        $(function() {
            $('#chartTime').click(function() {
                var color = Chart.helpers.color;

                function generateColor() {
                    return '#' + Math.floor(Math.random()*16777215).toString(16) + '99'
                }

                let start = $('#statistics_start').val()
                let end = $('#statistics_end').val()
                let user = $('#select-user').val()
                console.log(start + " - " + end)
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'post',
                    url: '{{ route('statistics.store') }}',
                    data: {start: start, end: end, user: user},
                    success: function(req) {
                        myChartEventTime.data.labels.splice(0,myChartEventTime.data.labels.length); 
                        myChartEventTime.data.datasets.splice(0,myChartEventTime.data.datasets.length);
                        for(i in req.dates){// Даты
                            myChartEventTime.data.labels.push(req.dates[i])
                        }
                        for(i in req.data){    
                            let newDataset = {
                                label: '[' + req.data[i].user.name + ']' + req.data[i].title,
                                backgroundColor: generateColor(),
                                borderColor: generateColor(),
                                data: req.data[i].difference,
                                fill: false,
                                border: 3
                            }
                            myChartEventTime.data.datasets.push(newDataset);
                        }
                        window.myChartEventTime.update();
                        //2
                        myChartEventCount.data.labels.splice(0,myChartEventCount.data.labels.length); 
                        myChartEventCount.data.datasets.splice(0,myChartEventCount.data.datasets.length);
                        for(i in req.dates){// Даты
                            myChartEventCount.data.labels.push(req.dates[i])
                        }
                        let arr = [];
                        $.each(req.count, function(index, value) {                            
                            $.each(value, function(index2, value2){                                
                                arr.push(value2)                                
                            })
                            var size = Object.keys(req.dates).length;
                            arr.splice(-1,1)
                            let newDataset = {
                                label: value.name,
                                backgroundColor: generateColor(),
                                borderColor: generateColor(),
                                data: arr,
                                fill: false,
                                border: 3
                            }
                            myChartEventCount.data.datasets.push(newDataset);
                            arr = []; 
                        })
                        window.myChartEventCount.update();

                        
                    }
                })

                
            })
        })




        var ctx = document.getElementById('myChartEventTime');
        var myChartEventTime = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [],
                datasets: []
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        var ctx = document.getElementById('myChartEventCount');
        var myChartEventCount = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [],
                datasets: []
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
                 


            
    </script>
</div>
@stop