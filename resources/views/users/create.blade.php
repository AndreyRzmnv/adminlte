@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')

@stop

@section('content')

@foreach ($errors->all() as $error)
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ $error }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endforeach
<div class="box box-primary p-2">
    <h3 class="box-title">Создание пользователя</h3>
    <form action="{{ route('users.store') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label>Имя</label>
            <input type="text" name="name" class="form-control " value="{{ old('name') }}" placeholder="Имя" autofocus="">
        </div>
         <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" class="form-control " value="{{ old('email') }}" placeholder="Email">
        </div>

         <div class="form-group">
            <label>Пароль</label>
            <input type="password" name="password" class="form-control" placeholder="Пароль">
        </div>
        <div class="form-group">
            <label>Роль</label>
            <select name="role" class="custom-select" required>
                @foreach($roles as $role)
                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                @endforeach
            </select>
            
        </div>
        <button type="submit" class="btn btn-primary">
            Регистрация
        </button>
    </form>
</div>
@stop