@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')

@stop

@section('content')
@foreach ($errors->all() as $error)
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ $error }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endforeach
<div class="box box-primary p-2">
    <h3 class="box-title">Редактирование пользователя</h3>
    <form action="{{ route('users.update', $user->id) }}" method="POST">
        <input type="hidden" name="_method" value="PATCH">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label>Имя</label>
            <input type="text" name="name" class="form-control" value="{{ $user->name }}" placeholder="Full name" autofocus="">
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" class="form-control" value="{{ $user->email }}" placeholder="Email">
        </div>
        <button type="submit" class="btn btn-primary">
            Обновить
        </button>
    </form>
</div>
@stop