@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    
@stop

@section('content')
    @include('flash::message')
    <div class="message"></div>
    <div class="box box-primary p-2">
        <h3 class="box-title">Список пользователей</h3>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <table id="testTable" class="table table-bordered table-striped dataTable" style="width:100%">
            <thead>
                <tr role="row">
                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 361px;">id</th>
                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 361px;">Имя</th>
                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 361px;">Email</th>
                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 361px;">Редактировать</th>
                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 361px;">Удалить</th>
                    
                </tr>
            </thead>
        </table>
    </div>
    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
        var table = $('#testTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('db.data') !!}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'email', name: 'email' },
                { data: 'edit', name: 'edit' },
                { data: 'delete', name: 'delete' }
            ]
        });
        function del(url) {
                //console.log(url)
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                
                $.ajax({
                    type: 'DELETE',
                    url: url,
                    success: function(data) {
                        table.ajax.reload();
                        $('.message').append("<div class='alert alert-primary' role='alert'>" + data + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>")
                    }
                })
                
            
        }
    </script>
@stop