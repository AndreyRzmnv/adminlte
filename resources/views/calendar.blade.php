@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    
@stop

@section('content')
    
    <style>
        .mes{
            position: fixed;
            z-index: 2;
            bottom: 2%;
            right: 2%;
        }
    </style>
    <div class="mes">
        <div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="false">
            <div class="toast-header">
                <strong class="title-event mr-auto"></strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                
            </div>
        </div>
    </div>





    <div>{{ auth()->user()->name }}</div>
    @role('Administrator|Manager')
    <div id="fullCalModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">Изменение
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">close</span></button>
                    <h4 id="modalTitle" class="modal-title"></h4>
                </div>
                <div id="modalBody" class="modal-body">
                    <label>Начало события</label>
                    <input class="form-control" id="edit_date_start" type="datetime-local" name="start"><br>
                    <label>Конец события</label>
                    <input class="form-control" id="edit_date_end" type="datetime-local" name="end"><br>
                    <label>Событие</label>
                    <input id="edit_event" class="form-control" type="text" name="title">
                </div>
                <div class="modal-footer">
                    <button id="success-but" class="btn btn-primary" data-dismiss="modal">Выполнено</button>
                    <button id="del-but" type="button" class="btn btn-default" data-dismiss="modal">Удалить</button>
                    <button id="update-but" class="btn btn-primary" data-dismiss="modal">Изменить</button>
                </div>
            </div>
        </div>
    </div>
    @endrole
    @role('Employee')
    <div id="fullCalModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">Статус
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">close</span></button>
                    <h4 id="modalTitle" class="modal-title"></h4>
                </div>
                <div id="modalBody" class="modal-body">
                    <label>Событие</label>
                    <input id="edit_event" class="form-control" type="text" name="title" disabled>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button id="success-but" class="btn btn-primary" data-dismiss="modal">Выполнено</button>
                </div>
            </div>
        </div>
    </div>
    @endrole
    
    @role('Administrator|Manager')
    <div class="err"></div>
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ $error }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endforeach
    
    
    
    <div class="container-fluid row">
        <div class="col-3 box box-primary p-2 m-2">
            <div class="success"></div>
            <div class="flsah"></div>
            <h3 class="box-title">Добавление событий</h3>            
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
            <label>Начало события</label>
            <input class="form-control" id="input_date_start" type="date" name="start"><br>
            <label>Конец события</label>
            <input class="form-control" id="input_date_end" type="date" name="end"><br>
            <label>Пользователь</label>
            <select id="select-user" name="role" class="custom-select" required>
                @foreach($users as $val)
                    <option value="{{ $val->id }}">{{ $val->name }}</option>
                @endforeach
            </select>
            <label>Событие</label>
            <div class="input-group">
                <input id="input_event" class="form-control" type="text" name="title">
                <div class="input-group-btn">
                <button id="add-new-event" type="submit" class="btn btn-primary btn-flat">Добавить</button>
                </div>
            </div>
            
        </div> 
        @endrole
        <div class="col box box-primary p-2 m-2">
            <div id='calendar'></div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
    <script type="text/javascript">

        let socket = io(':6002');
        socket.on('refresh', function() {
            console.log('refresh')
            calendar.refetchEvents();
        })
        socket.on('laravel_database_{{ auth()->user()->hash }}', function(data) {
            $('.title-event').text(data.start);
            $('.toast-body').text(data.title);
            $('.toast').toast('show')            
        })
        
         


        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            locale: 'ru',
            plugins: [ 'dayGrid', 'bootstrap', 'interaction', 'timeGrid' ],
            timeZone: 'UTC',
            themeSystem: 'bootstrap',
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth, timeGridWeek, timeGridDay'
            },
            eventColor: '#007bff',
            forceEventDuration: true, //принудительное end
            editable: true, //разрешение на изменение
            eventResizableFromStart: true,
            eventLimit: true,
            weekNumbers: true,
            eventLimit: true,
            dayNumbers: true,
            allDaySlot: true,
            eventSources: [
                {

                  url: '{{ route('calendar.view') }}',
                  type: 'GET'

                }
            ],
            
            
            eventDrop: function(info){
                
                let header = $('meta[name="csrf-token"]').attr('content')
                let title = info.event.title.toString()
                let start = info.event.start.toISOString()
                let end = info.event.end.toISOString()       
                title = title.split(']')[1];   
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'PATCH',
                    url: '/calendar/' + info.event.id,
                    data: {title: title, start: start, end: end},
                    success: function(data) {
                        //$('.err').append(data);
                        socket.emit('update')
                    }
                })
                
            },
            eventResize: function(info) {
                let title = info.event.title.toString()
                let start = info.event.start.toISOString()
                let end = info.event.end.toISOString()
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'PATCH',
                    url: '/calendar/' + info.event.id,
                    data: {title: title, start: start, end: end},
                    success: function(data) {
                    }
                })

            },
            eventClick: function(info) {
                //console.log(info)
                let start = info.event.start.toISOString()
                    let end = info.event.end.toISOString()
                    let id = info.event.id;
                if(info.event._def.extendedProps.status == 0){
                    $('#update-but').attr('val', id);
                    $('#success-but').attr('val', id);
                    $('#del-but').attr('val', id);
                    $('#edit_date_start').val(start.substr(0, 19));
                    $('#edit_date_end').val(end.substr(0, 19));
                    $('#edit_event').val(info.event.title.split(']')[1]);
                    $('#fullCalModal').modal();
                }
                
            },
            dateClick: function(info) {
                $('#input_date_start').val(info.dateStr)
                $('#input_date_end').val(info.dateStr)

                

            }
        
        });
        
        document.addEventListener('DOMContentLoaded', function() {
            calendar.render();
            
       
        
        
        //добавление нового ивента
        $('#add-new-event').click(function() {
            

            let title = $('#input_event').val()
            let start = $('#input_date_start').val()
            let end = $('#input_date_end').val()
            let user = $('#select-user').val()
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: '{{ route('calendar.store') }}',
                data: {title: title, start: start, end: end, user_id: user, autor_id: {{ $user->id }}},
                success: function(data) {
                    calendar.refetchEvents();
                    $('.success').append(data)
                    socket.emit('update')
                }
            })
            
            
        })
        //обновление ивента
        $('#update-but').click(function() {
            //calendar.getEventSources()
            //calendar.refetchEvents();
            let title = $('#edit_event').val()
            let start = $('#edit_date_start').val()
            let end = $('#edit_date_end').val()
            let id = $('#update-but').attr('val');
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
            $.ajax({
                type: 'PATCH',
                url: 'http://lrvl/calendar/' + id,
                data: {title: title, start: start, end: end},
                success: function(data) {
                    calendar.refetchEvents();
                    socket.emit('update')
                }
            })
        })

        $('#success-but').click(function() {
            let title = $('#edit_event').val()
            let id = $('#success-but').attr('val');
            var now = new Date().toISOString();
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
            $.ajax({
                type: 'PATCH',
                url: 'http://lrvl/calendar/' + id,
                data: {title: title, status: 1, staus_date: now.substr(0, 19)},
                success: function(data) {
                    calendar.refetchEvents();
                    socket.emit('update')
                }
            })
        })
        $('#del-but').click(function() {
            let id = $('#del-but').attr('val');
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
            $.ajax({
                type: 'DELETE',
                url: 'http://lrvl/calendar/' + id,
                success: function(data) {
                    calendar.refetchEvents();
                    socket.emit('update')
                }
            })



        })
        

    });
    function employeeUpdate(id, title) {

        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        $.ajax({
            type: 'PATCH',
            url: 'http://lrvl/calendar/' + id,
            data: {title: title, status: 1},
            success: function(data) {
                calendar.refetchEvents();

            }
        })
    }
    </script>
    
@stop