<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title_prefix', config('adminlte.title_prefix', ''))
@yield('title', config('adminlte.title', 'AdminLTE 3'))

@yield('title_postfix', config('adminlte.title_postfix', ''))</title>
    @if(! config('adminlte.enabled_laravel_mix'))
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/overlayScrollbars/css/OverlayScrollbars.min.css') }}">

    
    <!--datatables-->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <!--fulcalendar-->
    

    <link rel="stylesheet" href='{{ asset('fullcalendar-4.3.1/packages/core/main.css') }}' />
    <link rel="stylesheet" href='{{ asset('fullcalendar-4.3.1/packages/daygrid/main.css') }}' />
    <link rel="stylesheet" href='{{ asset('fullcalendar-4.3.1/packages/timegrid/main.css') }}' />
    <script src='{{ asset('fullcalendar-4.3.1/packages/core/main.js') }}' ></script>
    <script src='{{ asset('fullcalendar-4.3.1/packages/daygrid/main.js') }}' ></script>
    <script src='{{ asset('fullcalendar-4.3.1/packages/timegrid/main.js') }}' ></script>
    <!--fulcalendar-bootstrap-->
    <link rel="stylesheet" href='{{ asset('fullcalendar-4.3.1/packages/bootstrap/main.css') }}' />
    <script src='{{ asset('fullcalendar-4.3.1/packages/bootstrap/main.js') }}' ></script>
    <script src='{{ asset('fullcalendar-4.3.1/packages/interaction/main.js') }}' ></script>
<!--
    <script src='{{ asset('fullcalendar-4.3.1/packages/core/main.js') }}' ></script>
    <script src='{{ asset('fullcalendar-4.3.1/packages/daygrid/main.js') }}' ></script>
    <link href='{{ asset('fullcalendar-4.3.1/packages/core/main.css') }}' />
    <link href='{{ asset('fullcalendar-4.3.1/packages/daygrid/main.css') }}' />

    <link href='{{ asset('fullcalendar-4.3.1/packages/bootstrap/main.css') }}' />
    <link href='{{ asset('fullcalendar-4.3.1/packages/moment/main.css') }}' />

    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/fullcalendar.print.min.css') }}" media="print">
-->

    <!--adminlte-->
    <script src="{{ asset('vendor/adminlte/dist/js/js.js') }}"></script>
    @include('adminlte::plugins', ['type' => 'css'])

    @yield('adminlte_css_pre')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/adminlte-v2.4.17.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/adminlte.min.css') }}">
    
    @yield('adminlte_css')

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    @else
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @endif
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="@yield('classes_body')" @yield('body_data')>
@include('flash::message')
@yield('body')

@if(! config('adminlte.enabled_laravel_mix'))


<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('vendor/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>


@include('adminlte::plugins', ['type' => 'js'])

@yield('adminlte_js')
@else

<script src="{{ asset('js/app.js') }}"></script>
@endif

</body>
</html>
