<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/register', 'Auth\RegisterController@register')->name('logout');



Route::get('/home', function() {
	    return view('home');
})->name('home')->middleware('auth');//главная

//Route::resource('calendar', 'CalendarController')->middleware('auth');
//Route::post('calendarevent', 'CalendarController@create_event')->name('calendar.create_event');
Route::get('calendarview', 'CalendarController@view')->name('calendar.view')->middleware('auth');
Route::get('calendar', 'CalendarController@index')->middleware('auth');
Route::post('calendar', 'CalendarController@store')->name('calendar.store')->middleware('auth');
/*
Route::get('test', function()
{
	event(
		new App\Events\CalendarEvent(['massage' => 'data'])
	);
});

*/

Route::middleware('auth', 'role:Administrator,Manager')->group(function () {
	Route::resource('calendar', 'CalendarController');//календарь
	Route::post('calendar', 'CalendarController@create');
	Route::patch('/calendar/{calendar}', 'CalendarController@update');
});


Route::middleware('auth', 'role:Administrator')->group(function () {
	Route::resource('statistics', 'StatisticsController');
	Route::resource('users', 'UserController');
	Route::resource('calendar', 'CalendarController');//календарь
	Route::post('statisticsview', 'StatisticsController@view')->name('statistics.storeCount');
	Route::get('/datatables', 'UserController@data')->name('db.data');//таблица пользователей
	
});


Route::middleware('auth')->group(function () {
	Route::resource('calendar', 'CalendarController');//календарь
});

